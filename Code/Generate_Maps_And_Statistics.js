/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
// This script is used to compute statistics and maps for the paper
// "Climate vulnerability of Earth’s terrestrial biomes"
// 
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/



// Setup global variables and functions  
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

// Zoom out
Map.setCenter(0,0,2);

// Setup a polygon around the world for computational purposes
var unboundedGeo = ee.Geometry.Polygon([-180, 88, 0, 88, 180, 88, 180, -88, 0, -88, -180, -88], null, false);

// Set the viridis palette and visualization parameters for the biomes maps 
// these were taken from the RESOLVE Ecoregions app 
// https://ecoregions2017.appspot.com
var viridis = ["440154", "472D7B", "3B528B", "2C728E", "21908C", "27AD81", "5DC863", "AADC32", "FDE725"];
var viz = {min:1, max:14, palette:['5AA42F','CDCC74','98CB73','31714F','568772','86B6F0','F3AC3D','FFFE88','C6E6FC','D3C3A1','AAD5C3','EA3223','BF6C6A','E939BF']};

// Function that transforms a multiband image into an image collection
function BandsToCollection(image){
  var collection = ee.ImageCollection.fromImages(image.bandNames().map(function(bandName){
    return image.select(ee.String(bandName)).float().set('system:id', bandName).select([bandName], ['Band']);
  }));
  return collection;
}

// Function that computes the most probable biome distribution from 
// a multiband image
function consensusEstimate(multibandImage){
  var bandNames = multibandImage.bandNames();
  var multibandImageToIC = BandsToCollection(multibandImage)
                      .select([0],['NrOfPredictions'])
                      .map(function(biomeImage){
                        return biomeImage.addBands(ee.Image(ee.Number.parse(biomeImage.get('system:index')).add(1)).byte().rename('BiomeNr'));
                      });
  return multibandImageToIC.qualityMosaic('NrOfPredictions');
}

// Function that computes where multiple BCE states are possible by
// transforming the multiband image into a boolean image that shows
// areas where at least 2 biomes have a certain probability (set by confidenceLevel)
function multibiomeAreas (image){
  return BandsToCollection(image)
              .map(function(image){
                return image.gt(ee.Number(100).subtract(ee.Number(confidenceLevel)).multiply(10));
              })
              .sum()
              .gt(1);
}

// Function that computes the probabilistic changes between the current
// and a future scenario 
function probabilisticChange (currentImage,currentConsensusMask,futureMultibandImage){
  return BandsToCollection(currentImage.subtract(futureMultibandImage)
                                  .divide(1000)
                                  .mask(currentConsensusMask))
                                  .mosaic();
}

// Function that computes the determinisitc changes between the current
// and a future scenario. That means that the BCE will completely shift
// with a likelihood higher than the confidence level
function deterministicChange (currentConsensusMask,futureMultibandImage,confidenceLevel){
  return BandsToCollection(futureMultibandImage
                                  .divide(10)
                                  .mask(currentConsensusMask)
                                  .lte(ee.Number(100).subtract(ee.Number(confidenceLevel))))
                                  .mosaic();
}

// Function that computes the likely changes between the current
// and a future scenario. That means that the BCE will might shift
// with a likelihood higher a certain threshold
function likelyChange (currentConsensusMask,futureMultibandImage,threshold){
  return BandsToCollection(futureMultibandImage
                                  .divide(10)
                                  .mask(currentConsensusMask)
                                  .lte(ee.Number(threshold)))
                                  .mosaic();
}

// Function that computes binary multiband images of a biome image 
function binaryMultibandImage(biomeImage) {
  // Make a list of binary images 
  var listOfBinaryImages = ee.List.sequence(1,14).map(function(biomeNumber){
    return biomeImage.eq(ee.Image.constant(biomeNumber)).rename(ee.String('Biome_').cat(ee.Number(biomeNumber).int()));
  });
  // Function that transforms a list of images into a multiband image
  var mergeBands = function(image, previous) {
    return ee.Image(previous).addBands(image);
  };
  return ee.Image(listOfBinaryImages.iterate(mergeBands, ee.Image([])));
}

// Compute the overlapping areas between two images 
function computeOverlappingAreas(image1,image2){
  var fcWOverlappingArea = fcOfBiomes.map(function(f){
      var biomeX = ee.Number(f.get('Biome')).int();
      var listOfOverlappingAreas = ee.List.sequence(1,14).map(function(biomeY){
          var areaInCommon = image2.select(ee.Number(biomeY).subtract(1).int())
          .multiply(
            image1.select(biomeX.subtract(1))
            )
          .int()
          .multiply(ee.Image.pixelArea().divide(10000))
          .reduceRegion({reducer:ee.Reducer.sum(),
                         geometry:unboundedGeo,
                         crs:'EPSG:4326',
                         crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
                         maxPixels:1e12});
          return areaInCommon.get(ee.Image(image2.select(ee.Number(biomeY).subtract(1).int())).bandNames().get(0));
      });
  return f.set('AreaChanges',ee.Dictionary.fromLists(image2.bandNames(),listOfOverlappingAreas));
  });
return fcWOverlappingArea;
}

// Function that converts a histogram to a dictionary for export purposes
function histogramToDict (histogram,bandName){
  var keysForDict_AllBands = ee.Array(histogram.get(bandName)).slice(1, 0, 1).multiply(100).round().int().toList().flatten().map(function(n) {return ee.String('PercentBin_').cat(ee.String(n))});
	var valuesForDictNonNorm_AllBands = ee.Array(histogram.get(bandName)).slice(1, 1, 2).project([0]);
	var summedValues_AllBands = ee.Array(valuesForDictNonNorm_AllBands).project([0]).reduce('sum', [0]).repeat(0, keysForDict_AllBands.length());
	var valuesForDict_AllBands = valuesForDictNonNorm_AllBands.divide(summedValues_AllBands).toList().flatten();
  var dictForExportAllBands = ee.Dictionary.fromLists(keysForDict_AllBands, valuesForDict_AllBands);
  return dictForExportAllBands;
}



// Fig 1 - Observed and modelled biome distribution
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

// Load the RESOLVE biome distribution and the modelled image
var resolveBiomeDistribution = ee.Image('users/crowtherlab/Composite/Resolve_Biomes_1ArcSec');
var modelledCurrentImage = ee.Image('projects/crowtherlab/Publications/BiomeClimateVulnerability/currentDay_Coll_SummedImage_shortWindow');

// Create a consensus estimate of the modelled image 
var modelledBiomeDistribution = consensusEstimate(modelledCurrentImage);

// Map the layers
Map.addLayer(resolveBiomeDistribution, viz, 'Fig1 - RESOLVE biome distribution');
Map.addLayer(modelledBiomeDistribution.select(1), viz, 'Fig1 - modelled biome distribution');
Map.addLayer(ee.Image(0),{},'--------------------', false);



// Fig 2 - Model consensus estimates 
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

// Load the modelled images 
var modelled_rcp45_2050_Image = ee.Image('projects/crowtherlab/Publications/BiomeClimateVulnerability/rcp45_2050s_Coll_SummedImage_shortWindow');
var modelled_rcp45_2070_Image = ee.Image('projects/crowtherlab/Publications/BiomeClimateVulnerability/rcp45_2070s_Coll_SummedImage_shortWindow');
var modelled_rcp85_2050_Image = ee.Image('projects/crowtherlab/Publications/BiomeClimateVulnerability/rcp85_2050s_Coll_SummedImage_shortWindow');
var modelled_rcp85_2070_Image = ee.Image('projects/crowtherlab/Publications/BiomeClimateVulnerability/rcp85_2070s_Coll_SummedImage_shortWindow');

// Create consensus estimates of the modelled images 
var rcp45_2050_BiomeDistribution = consensusEstimate(modelled_rcp45_2050_Image);
var rcp45_2070_BiomeDistribution = consensusEstimate(modelled_rcp45_2070_Image);
var rcp85_2050_BiomeDistribution = consensusEstimate(modelled_rcp85_2050_Image);
var rcp85_2070_BiomeDistribution = consensusEstimate(modelled_rcp85_2070_Image);

// Identify areas with lower confidence. These are areas where at least 2 biomes 
// have a probability of greater than 10% to occur
var confidenceLevel = 90;

// Compute those multibiome areas 
var currentMultibiomeAreas = multibiomeAreas(modelledCurrentImage);
var rcp45_2070_multibiomeAreas = multibiomeAreas(modelled_rcp45_2070_Image);
var rcp85_2070_multibiomeAreas = multibiomeAreas(modelled_rcp85_2070_Image);

// Map the layers 
Map.addLayer(modelledBiomeDistribution.select(1), viz, 'Fig2 - modelled biome distribution',false);
Map.addLayer(currentMultibiomeAreas.selfMask(), {palette:['000000'], opacity:0.5}, 'Fig2 - multibiome areas (shading)',false),false;
Map.addLayer(rcp45_2070_BiomeDistribution.select(1), viz, 'Fig2 - biome distribution in 2070 under RCP 4.5', false);
Map.addLayer(rcp45_2070_multibiomeAreas.selfMask(), {palette:['000000'], opacity:0.5}, 'Fig2 - multibiome areas under RCP4.5 (shading)',false);
Map.addLayer(rcp85_2070_BiomeDistribution.select(1), viz, 'Fig2 - biome distribution in 2070 under RCP 8.5', false);
Map.addLayer(rcp85_2070_multibiomeAreas.selfMask(), {palette:['000000'], opacity:0.5}, 'Fig2 - multibiome areas under RCP8.5 (shading)',false);
Map.addLayer(ee.Image(0),{},'--------------------', false);

// Export the multibiome areas as shapefiles 
Export.table.toDrive(currentMultibiomeAreas.selfMask().reduceToVectorsStreaming({reducer:ee.Reducer.first(), geometry:unboundedGeo, crs:'EPSG:4326', crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88], maxPixels:1e13, tileScale:16}),'current_multibiomeAreas','GEE_Output',false,'SHP');
Export.table.toDrive(rcp45_2070_multibiomeAreas.selfMask().reduceToVectorsStreaming({reducer:ee.Reducer.first(), geometry:unboundedGeo, crs:'EPSG:4326', crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88], maxPixels:1e13, tileScale:16}),'rcp45_2070_multibiomeAreas','GEE_Output',false,'SHP');
Export.table.toDrive(rcp85_2070_multibiomeAreas.selfMask().reduceToVectorsStreaming({reducer:ee.Reducer.first(), geometry:unboundedGeo, crs:'EPSG:4326', crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88], maxPixels:1e13, tileScale:16}),'rcp85_2070_multibiomeAreas','GEE_Output',false,'SHP');

// Fig 3 - Changes in individual biome areas and histogram of consensus pixels
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

// Create a Feature Collection to hold the computed values
var fcOfBiomes = ee.FeatureCollection(ee.List.sequence(0,13).map(function(biome){return ee.Feature(ee.Geometry.Point([0,0])).set('Biome',ee.Number(biome).add(1))}));

// Combine the images of interest
var consensusImages = ee.Image.cat([modelledBiomeDistribution.select(1),
                        rcp45_2050_BiomeDistribution.select(1),
                        rcp45_2070_BiomeDistribution.select(1),
                        rcp85_2050_BiomeDistribution.select(1),
                        rcp85_2070_BiomeDistribution.select(1)
                        ])
                        .rename('current','rcp45_2050','rcp45_2070','rcp85_2050','rcp85_2070');

// Compute the areas of each biome and year per scenario 
var biomeAreas = fcOfBiomes.map(function(f){
  var consensusImages_masked = ee.Image([1,1,1,1,1,1,1,1,1]).mask(consensusImages.eq(ee.Image.constant(f.get('Biome'))))
                                .rename(consensusImages.bandNames());
  var dictOfValues = consensusImages_masked
                      .multiply(ee.Image.pixelArea().divide(10000))
                      .reduceRegion({reducer:ee.Reducer.sum(),
                                                          geometry:unboundedGeo,
                                                          crs:'EPSG:4326',
                                                          crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
                                                          maxPixels:1e12});
  return f.set(consensusImages.bandNames().get(0),dictOfValues.get(consensusImages.bandNames().get(0)))
          .set(consensusImages.bandNames().get(1),dictOfValues.get(consensusImages.bandNames().get(1)))
          .set(consensusImages.bandNames().get(2),dictOfValues.get(consensusImages.bandNames().get(2)))
          .set(consensusImages.bandNames().get(3),dictOfValues.get(consensusImages.bandNames().get(3)))
          .set(consensusImages.bandNames().get(4),dictOfValues.get(consensusImages.bandNames().get(4)));
});

// Export the computed values
Export.table.toDrive({
  collection: biomeAreas,
  description: 'export_BiomeAreas',
  folder: 'GEE_Output'
});

// Compute histograms of the prediction accurracy
var histogramCurrentImage = BandsToCollection(modelledCurrentImage).max().divide(1000)
                              .reduceRegion({
                              reducer: ee.Reducer.fixedHistogram(0, 1.05, 21),
                              geometry: unboundedGeo,
                              scale: 927.6624232772797,
                              maxPixels: 1e13,
                              tileScale: 16});
var histogram_rcp45_2070 = BandsToCollection(modelled_rcp45_2070_Image).max().divide(1000)
                              .reduceRegion({
                              reducer: ee.Reducer.fixedHistogram(0, 1.05, 21),
                              geometry: unboundedGeo,
                              scale: 927.6624232772797,
                              maxPixels: 1e13,
                              tileScale: 16});
var histogram_rcp85_2070 = BandsToCollection(modelled_rcp85_2070_Image).max().divide(1000)
                              .reduceRegion({
                              reducer: ee.Reducer.fixedHistogram(0, 1.05, 21),
                              geometry: unboundedGeo,
                              scale: 927.6624232772797,
                              maxPixels: 1e13,
                              tileScale: 16});

// Save the histograms in a FeatureCollection
var histogramToExport_current = ee.FeatureCollection(ee.Feature(ee.Geometry.Point([0, 0]))
                              .set(histogramToDict(histogramCurrentImage,'Band')));
var histogramToExport_rcp45_2070 = ee.FeatureCollection(ee.Feature(ee.Geometry.Point([0, 0]))
                              .set(histogramToDict(histogram_rcp45_2070,'Band')));
var histogramToExport_rcp85_2070 = ee.FeatureCollection(ee.Feature(ee.Geometry.Point([0, 0]))
                              .set(histogramToDict(histogram_rcp85_2070,'Band')));                              

// Export the histograms 
Export.table.toDrive(histogramToExport_current,'export_histogram_current','GEE_Output');
Export.table.toDrive(histogramToExport_rcp45_2070,'export_histogram_rcp45_2070','GEE_Output');
Export.table.toDrive(histogramToExport_rcp85_2070,'export_histogram_rcp85_2070','GEE_Output');



// Fig 4 - Probability of biome climate envelope turnover 
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

// Make a mask of the current consensus biome distribution 
var consensusMask = binaryMultibandImage(modelledBiomeDistribution.select(1));

// Compute the probabilistic changes in biome climate envelope turnover 
var probabilisticChange_rcp45_2070 = probabilisticChange(modelledCurrentImage,consensusMask,modelled_rcp45_2070_Image);
var probabilisticChange_rcp85_2070 = probabilisticChange(modelledCurrentImage,consensusMask,modelled_rcp85_2070_Image);
Map.addLayer(probabilisticChange_rcp45_2070,{min:0, max:1, palette:viridis},'Fig4 - probabilistic change under RCP4.5 in 2080', false);
Map.addLayer(probabilisticChange_rcp85_2070,{min:0, max:1, palette:viridis},'Fig4 - probabilistic change under RCP8.5 in 2080', false);
Map.addLayer(ee.Image(0),{},'--------------------', false);



// Fig S1 - Extent of Extrapolation vs Interpolation
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
var composite = ee.Image("projects/crowtherlab/Composite/CrowtherLab_Composite_30ArcSec"); 
var current_1994_2013_Mean = ee.Image("projects/crowtherlab/Publications/BiomeClimateVulnerability/Current_BioClim_1994_2013");
var topComp = composite.select(['EarthEnvTopoMed_Slope','EarthEnvTopoMed_Elevation','EarthEnvTopoMed_Eastness','EarthEnvTopoMed_Northness']);
var soilComp = composite.select(['SG_Bulk_density_015cm','SG_CEC_015cm','SG_Coarse_fragments_015cm','SG_Clay_Content_015cm','SG_Silt_Content_015cm','SG_Sand_Content_015cm','SG_H2O_Capacity_015cm']);
var totComp = current_1994_2013_Mean.addBands(soilComp).addBands(topComp);

// !! Decide on the climate scenario or composite to use
// var compForExtInt = ee.Image('projects/crowtherlab/Publications/BiomeClimateVulnerability/Future_BioClim_Ensembles/rcp85_2070s_mean');
var compForExtInt = totComp;

// Input the training samples
var trainingSamples = ee.FeatureCollection('projects/crowtherlab/Publications/BiomeClimateVulnerability/trainingSamples');

// Define the climate layers
var bandsToUse = compForExtInt.bandNames();

// Select the climate layers from the training data 
var fcForMinMax = trainingSamples.select(bandsToUse);

// Make a FC with the band names for better parallelization
var fcWithBandNames = ee.FeatureCollection(bandsToUse.map(function(bN) {
	return ee.Feature(ee.Geometry.Point([0, 0])).set('BandName', bN);
}));

// Map across every band of the collection to get the min and max values
var fcWithMinMaxValues = fcWithBandNames.map(function(fOI) {
	var bandBeingComputed = ee.Feature(fOI).get('BandName');
	var maxValueToSet = fcForMinMax.reduceColumns(ee.Reducer.minMax(), [bandBeingComputed]);
	return ee.Feature(fOI).set('MaxValue', maxValueToSet.get('max')).set('MinValue', maxValueToSet.get('min'));
});
var fcPrepped = fcWithMinMaxValues;

// Make two images from these values (a min and a max image)
var nameValueList = fcPrepped.reduceColumns(ee.Reducer.toList(), ['BandName']).get('list');
var maxValuesWNulls = fcPrepped.toList(100).map(function(f) {
	return ee.Feature(f).get('MaxValue');
});
var maxDict = ee.Dictionary.fromLists(nameValueList, maxValuesWNulls);
var minValuesWNulls = fcPrepped.toList(100).map(function(f) {
	return ee.Feature(f).get('MinValue');
});
var minDict = ee.Dictionary.fromLists(nameValueList, minValuesWNulls);
var minImage = minDict.toImage();
var maxImage = maxDict.toImage();

// Calculate a multiband image which shows the climate layers that are within the training range (univariatly)
var totalBandsBinary = compForExtInt.gte(minImage.select(bandsToUse)).and(compForExtInt.lte(maxImage.select(bandsToUse)));
// Map.addLayer(compForExtInt.gte(minImage.select(bandsToUse)),{},'Bands that are extrapolated within lower range',false);
// Map.addLayer(compForExtInt.lte(maxImage.select(bandsToUse)),{},'Bands that are extrapolated within upper range',false);
var totalBandsPercentage = totalBandsBinary.reduce('sum').divide(compForExtInt.bandNames().length());
// Map.addLayer(totalBandsPercentage, {palette: viridis, min: 0, max: 1}, 'Bands within training range', false);

// Export image to asset
Export.image.toAsset({
  image: totalBandsPercentage,
  description:"ExtentOfInterpolationUNIVAR_current",
  assetId:'projects/crowtherlab/Publications/BiomeClimateVulnerability/ExtentOfInterpolationUNIVAR_current',
  region:unboundedGeo,
  crs:'EPSG:4326',
  crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
  maxPixels:1e13
});

// Load the maps 
var extentOfInterpolation_current = ee.Image('projects/crowtherlab/Publications/BiomeClimateVulnerability/ExtentOfInterpolationUNIVAR_current');
var extentOfInterpolation_rcp45_2070 = ee.Image('projects/crowtherlab/Publications/BiomeClimateVulnerability/ExtentOfInterpolationUNIVAR_rcp45_2070s');
var extentOfInterpolation_rcp85_2070 = ee.Image('projects/crowtherlab/Publications/BiomeClimateVulnerability/ExtentOfInterpolationUNIVAR_rcp85_2070s');

// Map the extrapolation maps
Map.addLayer(extentOfInterpolation_current,{min:0, max:1, palette:viridis},'FigS1 - extent of interpolation for the current biome distribution model', false);
Map.addLayer(extentOfInterpolation_rcp45_2070,{min:0, max:1, palette:viridis},'FigS1 - extent of interpolation of climate conditions under RCP4.5 in 2080', false);
Map.addLayer(extentOfInterpolation_rcp85_2070,{min:0, max:1, palette:viridis},'FigS1 - extent of interpolation of climate conditions under RCP8.5 in 2080', false);
Map.addLayer(ee.Image(0),{},'--------------------', false);


// Fig S2 - Biome forecasts using different climate windows 
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
// Get the values for the long climate window 
var LW_modelled_rcp45_2070_Image = ee.Image('projects/crowtherlab/Publications/BiomeClimateVulnerability/rcp45_2070s_Coll_SummedImage_longWindow');
var LW_modelled_rcp85_2070_Image = ee.Image('projects/crowtherlab/Publications/BiomeClimateVulnerability/rcp85_2070s_Coll_SummedImage_longWindow');
var LW_rcp45_2070_BiomeDistribution = consensusEstimate(LW_modelled_rcp45_2070_Image);
var LW_rcp85_2070_BiomeDistribution = consensusEstimate(LW_modelled_rcp85_2070_Image);
var LW_rcp45_2070_multibiomeAreas = multibiomeAreas(LW_modelled_rcp45_2070_Image);
var LW_rcp85_2070_multibiomeAreas = multibiomeAreas(LW_modelled_rcp85_2070_Image);

Map.addLayer(rcp45_2070_BiomeDistribution.select(1), viz, 'FigS2 - short window biome distribution in 2070 under RCP 4.5', false);
Map.addLayer(rcp45_2070_multibiomeAreas.selfMask(), {palette:['000000'], opacity:0.5}, 'FigS2 - short window multibiome areas under RCP4.5 (shading)',false);
Map.addLayer(LW_rcp45_2070_BiomeDistribution.select(1), viz, 'FigS2 - long window biome distribution in 2070 under RCP 4.5', false);
Map.addLayer(LW_rcp45_2070_multibiomeAreas.selfMask(), {palette:['000000'], opacity:0.5}, 'FigS2 - long window multibiome areas under RCP4.5 (shading)',false);
Map.addLayer(rcp85_2070_BiomeDistribution.select(1), viz, 'FigS2 - short window biome distribution in 2070 under RCP 8.5', false);
Map.addLayer(rcp85_2070_multibiomeAreas.selfMask(), {palette:['000000'], opacity:0.5}, 'FigS2 - short window multibiome areas under RCP8.5 (shading)',false);
Map.addLayer(LW_rcp85_2070_BiomeDistribution.select(1), viz, 'FigS2 - long window biome distribution in 2070 under RCP 8.5', false);
Map.addLayer(LW_rcp85_2070_multibiomeAreas.selfMask(), {palette:['000000'], opacity:0.5}, 'FigS2 - long window multibiome areas under RCP8.5 (shading)',false);




// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// Tab 1 - Change in consensus biome areas

// Get binary images of the biomes as bands of a multiband image
var binaryMultibandImage_current = binaryMultibandImage(modelledBiomeDistribution.select(1));
var binaryMultibandImage_rcp45_2070 = binaryMultibandImage(rcp45_2070_BiomeDistribution.select(1));
var binaryMultibandImage_rcp85_2070 = binaryMultibandImage(rcp85_2070_BiomeDistribution.select(1));

// Compute the area changes by calculating the overlapping areas between 
// the current and future biome distributions
var areaChanges_current_RCP45_2070 = computeOverlappingAreas(binaryMultibandImage_current,binaryMultibandImage_rcp45_2070);
var areaChanges_current_RCP85_2070 = computeOverlappingAreas(binaryMultibandImage_current,binaryMultibandImage_rcp85_2070);

// Export
Export.table.toDrive({
  collection: areaChanges_current_RCP45_2070,
  description: 'export_areaChanges_current_RCP45_2070',
  folder: 'GEE_Output'
});
Export.table.toDrive({
  collection: areaChanges_current_RCP85_2070,
  description: 'export_areaChanges_current_RCP85_2070',
  folder: 'GEE_Output'
});



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// Statistics

// --- Total terrestrial surface area --- // 
var totalArea = currentMultibiomeAreas.gt(-999)
                              .multiply(ee.Image.pixelArea().divide(10000))
                              .reduceRegion({reducer:ee.Reducer.sum(),
                                             geometry:unboundedGeo,
                                             crs:'EPSG:4326',
                                             crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
                                             maxPixels:1e12});   
print(totalArea,'Total terrestrial surface area');

// --- Area classified as multibiome areas --- // 
var multibiomeArea_current = currentMultibiomeAreas.eq(1).selfMask()
                              .multiply(ee.Image.pixelArea().divide(10000))
                              .reduceRegion({reducer:ee.Reducer.sum(),
                                             geometry:unboundedGeo,
                                             crs:'EPSG:4326',
                                             crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
                                             maxPixels:1e12});
var multibiomeArea_rcp45_2070 = rcp45_2070_multibiomeAreas.eq(1).selfMask()
                              .multiply(ee.Image.pixelArea().divide(10000))
                              .reduceRegion({reducer:ee.Reducer.sum(),
                                             geometry:unboundedGeo,
                                             crs:'EPSG:4326',
                                             crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
                                             maxPixels:1e12});
var multibiomeArea_rcp85_2070 = rcp85_2070_multibiomeAreas.eq(1).selfMask()
                              .multiply(ee.Image.pixelArea().divide(10000))
                              .reduceRegion({reducer:ee.Reducer.sum(),
                                             geometry:unboundedGeo,
                                             crs:'EPSG:4326',
                                             crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
                                             maxPixels:1e12});
print(multibiomeArea_current,'Area that is currently a multibiome area');
print(multibiomeArea_rcp45_2070,'Area that is a multibiome area under RCP4.5 in 2080');
print(multibiomeArea_rcp85_2070,'Area that is a multibiome area under RCP8.5 in 2080');

// --- Area that will change their BCE with >90% confidence --- // 
var areasWithDeterministicBiomeChanges_rcp45_2070 = deterministicChange(consensusMask,modelled_rcp45_2070_Image,confidenceLevel);
var areasWithDeterministicBiomeChanges_rcp85_2070 = deterministicChange(consensusMask,modelled_rcp85_2070_Image,confidenceLevel);

var areaOfDeterministicChange_rcp45_2070 = areasWithDeterministicBiomeChanges_rcp45_2070.eq(1).selfMask()
                              .multiply(ee.Image.pixelArea().divide(10000))
                              .reduceRegion({reducer:ee.Reducer.sum(),
                                             geometry:unboundedGeo,
                                             crs:'EPSG:4326',
                                             crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
                                             maxPixels:1e12});
var areaOfDeterministicChange_rcp85_2070 = areasWithDeterministicBiomeChanges_rcp85_2070.eq(1).selfMask()
                              .multiply(ee.Image.pixelArea().divide(10000))
                              .reduceRegion({reducer:ee.Reducer.sum(),
                                             geometry:unboundedGeo,
                                             crs:'EPSG:4326',
                                             crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
                                             maxPixels:1e12});
print(areaOfDeterministicChange_rcp45_2070,'Area undergoing a deterministic change under RCP4.5 in 2080');
print(areaOfDeterministicChange_rcp85_2070,'Area undergoing a deterministic change under RCP8.5 in 2080');

// --- Area that could change their BCE with >70% confidence --- // 
var threshold = 70;
var areasWithLikelyBiomeChanges_rcp45_2070 = likelyChange(consensusMask,modelled_rcp45_2070_Image,threshold);
var areasWithLikelyBiomeChanges_rcp85_2070 = likelyChange(consensusMask,modelled_rcp85_2070_Image,threshold);

var areaOfLikelyChange_rcp45_2070 = areasWithLikelyBiomeChanges_rcp45_2070.eq(1).selfMask()
                              .multiply(ee.Image.pixelArea().divide(10000))
                              .reduceRegion({reducer:ee.Reducer.sum(),
                                             geometry:unboundedGeo,
                                             crs:'EPSG:4326',
                                             crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
                                             maxPixels:1e12});
var areaOfLikelyChange_rcp85_2070 = areasWithLikelyBiomeChanges_rcp85_2070.eq(1).selfMask()
                              .multiply(ee.Image.pixelArea().divide(10000))
                              .reduceRegion({reducer:ee.Reducer.sum(),
                                             geometry:unboundedGeo,
                                             crs:'EPSG:4326',
                                             crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
                                             maxPixels:1e12});
print(areaOfLikelyChange_rcp45_2070,'Area undergoing a likely change under RCP4.5 in 2080');
print(areaOfLikelyChange_rcp85_2070,'Area undergoing a likely change under RCP8.5 in 2080');






// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// Export the images 
// Fig1 - RESOLVE biome distribution
Export.image.toDrive({
  image: resolveBiomeDistribution.cast({Band:'int16'}),
  description:"Fig1_currentBiomeDistribution",
  folder:'GEE_OUTPUT',
  region:unboundedGeo,
  crs:'EPSG:4326',
  crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
  maxPixels:1e13
});

// Fig1 - modelled biome distribution
Export.image.toDrive({
  image: modelledBiomeDistribution.select(1).cast({Band:'byte'}),
  description:"Fig1_modelledBiomeDistribution",
  folder:'GEE_OUTPUT',
  region:unboundedGeo,
  crs:'EPSG:4326',
  crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
  maxPixels:1e13
});

// Fig2 - modelled biome distribution under RCP4.5 in 2070
Export.image.toDrive({
  image: rcp45_2070_BiomeDistribution.select(1).cast({Band:'byte'}),
  description:"Fig2_modelledBiomeDistribution_rcp45_2070",
  folder:'GEE_OUTPUT',
  region:unboundedGeo,
  crs:'EPSG:4326',
  crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
  maxPixels:1e13
});

// Fig2 - modelled biome distribution under RCP8.5 in 2070
Export.image.toDrive({
  image: rcp85_2070_BiomeDistribution.select(1).cast({Band:'byte'}),
  description:"Fig2_modelledBiomeDistribution_rcp85_2070",
  folder:'GEE_OUTPUT',
  region:unboundedGeo,
  crs:'EPSG:4326',
  crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
  maxPixels:1e13
});

// Fig2 - current multibiome areas
Export.image.toDrive({
  image: currentMultibiomeAreas.cast({Band:'int16'}),
  description:"Fig2_multibiomeAreas_current",
  folder:'GEE_OUTPUT',
  region:unboundedGeo,
  crs:'EPSG:4326',
  crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
  maxPixels:1e13
});

// Fig2 - multibiome areas under RCP4.5 in 2070
Export.image.toDrive({
  image: rcp45_2070_multibiomeAreas.cast({Band:'int16'}),
  description:"Fig2_multibiomeAreas_rcp45_2070",
  folder:'GEE_OUTPUT',
  region:unboundedGeo,
  crs:'EPSG:4326',
  crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
  maxPixels:1e13
});

// Fig2 - multibiome areas under RCP8.5 in 2070
Export.image.toDrive({
  image: rcp85_2070_multibiomeAreas.cast({Band:'int16'}),
  description:"Fig2_multibiomeAreas_rcp85_2070",
  folder:'GEE_OUTPUT',
  region:unboundedGeo,
  crs:'EPSG:4326',
  crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
  maxPixels:1e13
});

// Fig4 - multibiome areas under RCP4.5 in 2070
Export.image.toDrive({
  image: probabilisticChange_rcp45_2070.cast({Band:'float'}),
  description:"Fig4_probabilisticChange_rcp45_2070",
  folder:'GEE_OUTPUT',
  region:unboundedGeo,
  crs:'EPSG:4326',
  crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
  maxPixels:1e13
});

// Fig4 - multibiome areas under RCP8.5 in 2070
Export.image.toDrive({
  image: probabilisticChange_rcp85_2070.cast({Band:'float'}),
  description:"Fig4_probabilisticChange_rcp85_2070",
  folder:'GEE_OUTPUT',
  region:unboundedGeo,
  crs:'EPSG:4326',
  crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
  maxPixels:1e13
});

// FigS1 - Extent of interpolation for current biome distribution model 
Export.image.toDrive({
  image: extentOfInterpolation_current.cast({Band:'float'}),
  description:"Fig4_extentOfInterpolation_current",
  folder:'GEE_OUTPUT',
  region:unboundedGeo,
  crs:'EPSG:4326',
  crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
  maxPixels:1e13
});

// FigS1 - Extent of interpolation of climate bands for RCP4.5 in 2070
Export.image.toDrive({
  image: extentOfInterpolation_rcp45_2070.cast({Band:'float'}),
  description:"Fig4_extentOfInterpolation_rcp45_2070",
  folder:'GEE_OUTPUT',
  region:unboundedGeo,
  crs:'EPSG:4326',
  crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
  maxPixels:1e13
});

// FigS1 - Extent of interpolation of climate bands for RCP8.5 in 2070
Export.image.toDrive({
  image: extentOfInterpolation_rcp85_2070.cast({Band:'float'}),
  description:"Fig4_extentOfInterpolation_rcp85_2070",
  folder:'GEE_OUTPUT',
  region:unboundedGeo,
  crs:'EPSG:4326',
  crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
  maxPixels:1e13
});

// FigS1 - Extent of interpolation (univariately)
Export.image.toDrive({
  image: totalBandsPercentage.cast({Band:'float'}),
  description:"FigS1_extentOfInterpolationUNIVAR_current",
  folder:'GEE_OUTPUT',
  region:unboundedGeo,
  crs:'EPSG:4326',
  crsTransform:[0.008333333333333333,0,-180,0,-0.008333333333333333,88],
  maxPixels:1e13
});


print('--- Areas as percentages ---');
print(1263740258.639524/13138715924.880714,'Area that is currently a multibiome area');
print(2115913319.4651856/13138715924.880714,'Area that is a multibiome area under RCP4.5 in 2080');
print(2510390319.065644/13138715924.880714,'Area that is a multibiome area under RCP8.5 in 2080');
print(1500904966.8401694/13138715924.880714,'Area undergoing a deterministic change under RCP4.5 in 2080');
print(2196397690.2473984/13138715924.880714,'Area undergoing a deterministic change under RCP8.5 in 2080');
print(2761897736.545067/13138715924.880714,'Area undergoing a likely change under RCP4.5 in 2080');
print(3644668955.1036296/13138715924.880714,'Area undergoing a likely change under RCP8.5 in 2080');