## This repository contains the code used for the analyses in the paper "Climate vulnerability of Earth’s terrestrial biomes"
### The repo is organized as follows:
- **Code** contains all the code that was used for the analysis and to generate figures
    - *Forcasting_Earths_Biomes.ipynb* runs the whole analysis
    - *Generate_Maps_And_Statistics.js* was used to compute all the spatial statistics and to generate the final maps (incl. the extent of extrapolation vs interpolation)
    - *Figures.R* is used to generate the figures and tables
- **Data** contains all the data necessary to either run a script or to generate the figures
- **Figures** contains all the figures for the paper

________________________________________________________________________________________________

### The general method of the paper is the following:
We calibrate probabilistic machine learning models to classify the Earth’s fourteen biomes at global scale using soil, topographic, and climate drivers and assess its accuracy. By doing so we identify the biome-climate envelopes (BCEs) that govern the abiotic component of the Earth’s biomes. We then apply this model to global climate change scenarios to predict Earth’s BCEs in the future under RCP 4.5 and 8.5 scenarios, a conservative and most likely potential climate future. Using this approach we assess the vulnerability of Earth’s terrestrial BCEs to future global change.

1. we use the RESOLVE global biome product (Dinerstein et al., 2017) as training, validation and test datasets for our biome classification model
2. we tune the hyperparameters of the model and evaluate it
3. we use the hyperparameters to train an ensemble of 1000 random forest models on randomly sampled training datasets each selected with a different random seed, and each with 2,500 random point locations per biome
4. we generate 1,000 biome predictions per pixel by classifying every pixel in the environmental composite layer stack using either 
    - present climate or
    - future climate (RCP4.5 and RCP8.5)
5. consensus biome classifications were made using the most frequent biome prediction within each pixel
